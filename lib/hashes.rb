# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  lengths = Hash.new(0)
  str.split.uniq.each {|word|  lengths[word] = word.length}
  lengths
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by{|k,v| v}[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  #iterate through newer
  #make appropriate changes to older with each iteration of newer
  newer.each {|k,v|older[k] = v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  frequency = Hash.new(0)
  word.each_char {|char| frequency[char]+=1}
  frequency
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  frequency = Hash.new(0)
  arr.each {|el| frequency[el] += 1}
  frequency.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  frequency = Hash.new(0)
  numbers.each do |num|
    frequency[:even]+=1 if num.even?
    frequency[:odd]+=1 if num.odd?
  end
  frequency
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = ["a","e","i","o","u"]
  vowel_frequency = Hash.new(0)
  string.each_char do |char|
    vowel_frequency[char] += 1 if vowels.include?(char)
  end
  vowel_frequency.sort_by {|k,v|v}[-1].first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  #filter out students with birthdays in the second half of the year
  combinations = []
  second_half_birthdays = students.select {|k,v| v > 6}.keys
  second_half_birthdays.each_index do |idx1|
    #idx2 = idx1 + 1
    #while idx2 < second_half_birthdays.length
    (idx1+1...second_half_birthdays.length).each do |idx2|
      combinations << [second_half_birthdays[idx1],second_half_birthdays[idx2]]
    #  idx2 += 1
    end
  end
  combinations
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  specimen_counter = Hash.new(0)
  specimens.each {|specimen| specimen_counter[specimen]+=1}
  number_of_species = specimen_counter.length
  smallest_population_size = specimen_counter.values.min
  largest_population_size = specimen_counter.values.max

  number_of_species**2 * smallest_population_size / largest_population_size
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  #iterate over vandalized_sign, check if normal sign a. has the key, and b. has a greater or equal to value
  normal_sign_counter = character_count(normal_sign)
  vandalized_sign_counter = character_count(vandalized_sign)
  vandalized_sign_counter.each do |k,v|
    if normal_sign_counter.has_key?(k)
      return false if normal_sign_counter[k] < v
    else
      return false
    end
  end
  return true
end

def character_count(str)
  character_counter = Hash.new(0)
  str.each_char {|char| character_counter[char.downcase]+=1}
  character_counter
end
